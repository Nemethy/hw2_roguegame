﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Szabalyok;
using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Automatizmus;

namespace RogueGame.Jatek.Keret
{
    class Keret
    {
        const int PALYA_MERET_X = 21;
        const int PALYA_MERET_Y = 11;
        const int KINCSEK_SZAMA = 10;
        private JatekTer jt;
        private bool jatekVege = false;
        OrajelGenerator generator;
        int megtalaltKincsek;

        public Keret()
        {
            generator = new OrajelGenerator();
            jt = new JatekTer(PALYA_MERET_X, PALYA_MERET_Y); 
            PalyaGeneralas();
        }

        private void PalyaGeneralas()
        {
            Random r = new Random();

            for (int i = 0; i < PALYA_MERET_X; i++)
            {
                new Fal(i, 0, jt);  // első oszlop
                new Fal(i, PALYA_MERET_Y - 1, jt);  // utolsó oszlop
                if (i > 0 && i < PALYA_MERET_Y - 1) //x nagyobb, mint y
                {
                    new Fal(0, i, jt);
                    new Fal(PALYA_MERET_X - 1, i, jt);
                }
            }   

            for (int i = 0; i < KINCSEK_SZAMA;)
            {
                int x = r.Next(1, PALYA_MERET_X - 1), y = r.Next(1, PALYA_MERET_Y - 1);
                if (jt.MegadottHelyenLevok(x, y).Length == 0 && (x != 1 || y != 1))
                {
                    new Kincs(x, y, jt).KincsFelvetel += KincsFelvetelTortent;
                    ++i;
                }
            }

        }

        public void Futtatas()
        {
            KonzolosMegjelenito km1 = new KonzolosMegjelenito(jt, 0, 0);
            Jatekos jatekos = new Jatekos("Béla", 1, 1, jt);
            jatekos.JatekosValtozas += JatekosValtozasTortent;
            GepiJatekos gepiJatekos = new GepiJatekos("Kati", 6, 1, jt);
            GonoszGepiJatekos gonoszGepiJatekos = new GonoszGepiJatekos("Laci", 9, 8, jt);
            KonzolosMegjelenito km2 = new KonzolosMegjelenito(jatekos, 25, 0);
            KonzolosEredmenyAblak kea = new KonzolosEredmenyAblak(0, 12, 5);
            kea.JatekosFeliratkozas(jatekos);

            generator.Felvetel(gepiJatekos);
            generator.Felvetel(gonoszGepiJatekos);
            generator.Felvetel(km1);
            generator.Felvetel(km2);

            do // rossz a sorrend?
            {
                //km1.Megjelenites();
                //gepiJatekos.Mozgas();
                //gonoszGepiJatekos.Mozgas();
                try
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.LeftArrow) jatekos.Megy(-1, 0);
                    if (key.Key == ConsoleKey.RightArrow) jatekos.Megy(1, 0);
                    if (key.Key == ConsoleKey.UpArrow) jatekos.Megy(0, -1);
                    if (key.Key == ConsoleKey.DownArrow) jatekos.Megy(0, 1);
                    if (key.Key == ConsoleKey.Escape) jatekVege = true;
                }
                catch (MozgasHelyhianyMiattNemSikerult e)
                {
                    System.Console.Beep(500 + e.Elemek.Length * 100, 10);
                }
                
                //km2.Megjelenites();
            } while (!jatekVege);

            Console.ReadLine();
        }

        void KincsFelvetelTortent(Kincs k, Jatekos j)
        {
            jatekVege = ++megtalaltKincsek == KINCSEK_SZAMA ? true : false;  
        }

        void JatekosValtozasTortent(Jatekos j, int ujPont, int ujElet)
        {
            jatekVege = ujElet == 0 ? true : false; 
        }
    }
}
