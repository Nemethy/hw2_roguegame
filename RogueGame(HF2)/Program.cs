﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Keret;

namespace RogueGame_HF2_
{
    class Program
    {
        static void Main(string[] args)
        {   /* // (1. tesztelés, itt még nem abstract volt a JatekElem osztály
            JatekTer jt = new JatekTer(10, 10);
            JatekElem je1 = new JatekElem(1, 1, jt);
            JatekElem je2 = new JatekElem(2, 4, jt);
            JatekElem je3 = new JatekElem(7, 8, jt);

            jt.Torles(je1);
            Console.WriteLine("A (2, 4)-es koordinátán {0} játékos található.", jt.MegadottHelyenLevok(2, 4).Length);
            foreach (JatekElem item in jt.MegadottHelyenLevok(2, 7, 6))
            {
                Console.WriteLine("(" + item.X + "," + item.Y + ")");
            }
            */

            // (2. tesztelés)
            Keret k = new Keret();
            k.Futtatas();
        }
    }
}
