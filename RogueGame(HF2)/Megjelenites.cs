﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Automatizmus;
using RogueGame.Jatek.Szabalyok;

namespace RogueGame.Jatek.Megjelenites
{
    interface IKirajzolhato
    {
        int X { get; }
        int Y { get; }
        char Alak { get; }
    }

    interface IMegjelenitheto
    {
        int[] MegjelenitendoMeret { get; }
        IKirajzolhato[] MegjelenitendoElemek();
    }

    class KonzolosMegjelenito : IAutomatikusanMukodo
    {
        IMegjelenitheto forras;
        int pozX;
        int pozY;

        public KonzolosMegjelenito(IMegjelenitheto forras, int pozX, int pozY)
        {
            this.forras = forras;
            this.pozX = pozX;
            this.pozY = pozY;
        }

        public int MukodesIntervallum => 1;

        public void Megjelenites() //kiolvassa a forras-ból a kirajzolandó elemeket, majd ezeket kirajzolja a pozX és pozY által eltolt helyre
        {
            int[] meretek = forras.MegjelenitendoMeret;

            bool ottVan;
            for (int i = 0; i < meretek[0]; i++)
                for (int j = 0; j < meretek[1]; j++)
                {
                    ottVan = false;
                    foreach (var item in forras.MegjelenitendoElemek())
                        if (item.X == i && item.Y == j)
                        {
                            ottVan = true;
                            SzalbiztosKonzol.KiirasXY(i + pozX, j + pozY, item.Alak); // ? SzalbiztosKonzol.KiirasXY(i, j, item.Alak);
                        }

                    if (!ottVan)
                        SzalbiztosKonzol.KiirasXY(i, j, ' ');
                }
        }

        public void Mukodik()
        {
            Megjelenites();
        }
    }

    class KonzolosEredmenyAblak
    {
        int pozX;
        int pozY;
        int maxSorSzam;
        int sor = 0;

        public KonzolosEredmenyAblak(int pozX, int pozY, int maxSorSzam)
        {
            this.pozX = pozX;
            this.pozY = pozY;
            this.maxSorSzam = maxSorSzam;
        }

        void JatekosValtozasTortent(Jatekos j, int ujPont, int ujElet)
        {
            SzalbiztosKonzol.KiirasXY(pozX, pozY + sor, string.Format("Játékos neve: {0}, pontszáma: {1}, életereje: {2}", j.Nev, ujPont, ujElet));
            sor = (sor + 1) % (maxSorSzam + 1);
        }

        public void JatekosFeliratkozas(Jatekos j)
        {
            j.JatekosValtozas += JatekosValtozasTortent;
        }
    }
}
