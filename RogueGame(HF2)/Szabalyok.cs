﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Automatizmus;

namespace RogueGame.Jatek.Szabalyok
{
    delegate void KincsFelvetelKezelo(Kincs k, Jatekos j);
    delegate void JatekosValtozasKezelo(Jatekos j, int ujPont, int ujElet);

    class Fal : RogzitettJatekElem, IKirajzolhato
    {
        public override float Meret { get { return 1; } }

        public char Alak => '\u2593';

        public Fal(int x, int y, JatekTer jt) : base(x, y, jt) { }

        public override void Utkozes(JatekElem je) { }
    }

    class Kincs : RogzitettJatekElem, IKirajzolhato
    {
        public event KincsFelvetelKezelo KincsFelvetel; // eseménykezelő

        public override float Meret { get => 1; }
        public char Alak => '\u2666';

        public Kincs(int x, int y, JatekTer jt) : base(x, y, jt) { }

        public override void Utkozes(JatekElem je)
        {
            if(je is Jatekos)
            {
                (je as Jatekos).PontotSzerez(50);
                KincsFelvetel?.Invoke(this, je as Jatekos);
                jt.Torles(this);
            }
        }


    }

    class Jatekos : MozgoJatekElem, IKirajzolhato, IMegjelenitheto
    {
        public event JatekosValtozasKezelo JatekosValtozas;

        string nev;
        int eletero = 100;
        int pontszam = 0;

        public string Nev { get => nev; }
        public override float Meret { get { return 0.2f; } }
        public virtual char Alak => Aktiv ? '\u263A' : '\u263B';

        public int[] MegjelenitendoMeret
        {
            get
            {
                return jt.MegjelenitendoMeret;
            }
        }

        public Jatekos(string nev, int x, int y, JatekTer jt) : base(x, y, jt)
        {
            this.nev = nev;
            Aktiv = true;
        }

        public override void Utkozes(JatekElem je) { }

        public void Serul(int sebzes)
        {
            if (sebzes != 0 && this.Aktiv)
                JatekosValtozas?.Invoke(this, pontszam, eletero -= eletero > sebzes ? sebzes : 0);
            if (eletero == 0)
                Aktiv = false;
        }

        public void PontotSzerez(int pont)
        {
            if (pont != 0)
                JatekosValtozas?.Invoke(this, pontszam += pont, eletero);
        }

        public void Megy(int rx, int ry)
        {
            AtHelyez(X + rx, Y + ry);
        }

        public IKirajzolhato[] MegjelenitendoElemek() // a játékos a pálya 5 sugarú környezetétlátja be, így az itt található elemeket fogja visszaadni.
        {
            JatekElem[] elemek = jt.MegadottHelyenLevok(this.X, this.Y, 5);
            int count = 0;

            foreach (var item in elemek)
                if (item is IKirajzolhato)
                    count++;

            IKirajzolhato[] vissza = new IKirajzolhato[count];
            count = 0;
            foreach (var item in elemek)
                if (item is IKirajzolhato)
                    vissza[count++] = item as IKirajzolhato;

            return vissza;
        }

        
    }

    class GepiJatekos : Jatekos, IAutomatikusanMukodo
    {
        static Random rnd = new Random();
        public override char Alak => '\u2640';

        public int MukodesIntervallum => 2;

        public GepiJatekos(string nev, int x, int y, JatekTer jt) : base(nev, x, y, jt) { }
        
        public void Mozgas()
        {
            try
            {
                switch (rnd.Next(0, 4))
                { 
                    case 0: Megy(-1, 0); break;
                    case 1: Megy(1, 0); break;
                    case 2: Megy(0, -1); break;
                    case 3: Megy(0, 1); break;
                    default: break;
                }
            }catch(MozgasHelyhianyMiattNemSikerult)
            {
                this.Mozgas();
            }
        }

        public void Mukodik()
        {
            Mozgas();
        }
    }

    class GonoszGepiJatekos : GepiJatekos
    {
        public override char Alak => '\u2642';

        public GonoszGepiJatekos(string nev, int x, int y, JatekTer jt) : base(nev, x, y, jt) { }
        
        public override void Utkozes(JatekElem elem)
        {
            base.Utkozes(elem);
            if (elem is Jatekos && Aktiv)
                (elem as Jatekos).Serul(10);
        }
    }

    class MozgasNemSikerultKivetel : Exception
    {
        JatekElem jatekElem;
        int x;
        int y;

        public JatekElem JatekElem { get => jatekElem; }
        public int X { get => x; }
        public int Y { get => y; }

        public MozgasNemSikerultKivetel(JatekElem jatekElem, int x, int y)
        {
            this.jatekElem = jatekElem;
            this.x = x;
            this.y = y;
        }
    }

    class MozgasHalalMiattNemSikerultKivetel : MozgasNemSikerultKivetel
    {
        public MozgasHalalMiattNemSikerultKivetel(JatekElem jatekElem,
            int x, int y) : base(jatekElem, x, y) { }
    }

    class MozgasHelyhianyMiattNemSikerult : MozgasNemSikerultKivetel
    {
        JatekElem[] elemek;

        public JatekElem[] Elemek { get => elemek; }

        public MozgasHelyhianyMiattNemSikerult(JatekElem jatekElem, 
            int x, int y, JatekElem[] elemek) : base(jatekElem, x, y)
        {
            this.elemek = elemek;
        }
    }
}
