﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Szabalyok;

namespace RogueGame.Jatek.Jatekter
{
    abstract class JatekElem
    {
        int x;
        int y;
        protected JatekTer jt;
        public abstract float Meret { get; }

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y { get => y; set => y = value; }

        public JatekElem(int x, int y, JatekTer jt)
        {
            this.x = x;
            this.y = y;
            this.jt = jt;
            jt.Felvetel(this);
        }

        public abstract void Utkozes(JatekElem je);
    }

    abstract class RogzitettJatekElem : JatekElem
    {
        public RogzitettJatekElem(int x, int y, JatekTer jt) : base(x, y, jt) { }
        
        public override float Meret => throw new NotImplementedException();

        public override void Utkozes(JatekElem je) { }
    }

    class MozgoJatekElem : JatekElem
    {
        private bool aktiv;

        public MozgoJatekElem(int x, int y, JatekTer jt) : base(x, y, jt) { }

        public bool Aktiv { get => aktiv; set => aktiv = value; }

        public override float Meret => throw new NotImplementedException();

        public void AtHelyez(int ujX, int ujY)
        {
            foreach (var item in jt.MegadottHelyenLevok(ujX, ujY, 0))
            {
                item.Utkozes(this);
                this.Utkozes(item);
                if (aktiv == false)
                    throw new MozgasHalalMiattNemSikerultKivetel(this, ujX, ujY);
            }
            if (aktiv)
            {
                float ossz = 0.0f;
                foreach (var item in jt.MegadottHelyenLevok(ujX, ujY, 0))
                {
                    ossz += item.Meret;
                }

                if (ossz + this.Meret <= 1)
                {
                    this.X = ujX;
                    this.Y = ujY;
                }
                else throw new MozgasHelyhianyMiattNemSikerult(this, ujX, ujY, jt.MegadottHelyenLevok(ujX, ujY, 0));
            }
        }

        public override void Utkozes(JatekElem je)
        {
            throw new NotImplementedException();
        }
    }

    class JatekTer : IMegjelenitheto
    {
        const int MAX_ELEMSZAM = 1000;
        //private int elemN;
        private List<JatekElem> elemek = new List<JatekElem>();
        private int meretX;
        private int meretY;

        public int MeretY { get => meretY; }
        public int MeretX { get => meretX; }

        public int[] MegjelenitendoMeret // property
        {
            get
            {
                int[] meretek = new int[2];
                meretek[0] = meretX;
                meretek[1] = meretY;

                return meretek;
            }   
        }

        public JatekTer(int meretX, int meretY)
        {
            this.meretX = meretX;
            this.meretY = meretY;
        }

        public void Felvetel(JatekElem jatekElem)
        {
            elemek.Add(jatekElem);
        }
        public void Torles(JatekElem jatekElem)
        {
            elemek.Remove(jatekElem);
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y, int tavolsag)
        {
            JatekElem[] array;
            int db = 0;
            foreach (JatekElem item in elemek)
            {
                if (Math.Sqrt(Math.Pow((item.X - x), 2) + Math.Pow((item.Y - y), 2) ) <= tavolsag)
                    db++;
            }
            array = new JatekElem[db];

            foreach (JatekElem item in elemek)
            {
                if (Math.Sqrt(Math.Pow((item.X - x), 2) + Math.Pow((item.Y - y), 2)) <= tavolsag)
                    array[--db] = item;
            }

            return array;
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y)
        {
            return MegadottHelyenLevok(x, y, 0);
        }

        public IKirajzolhato[] MegjelenitendoElemek()
        {
            int count = 0;
            foreach (var item in elemek)
                if(item is IKirajzolhato)
                    count++;

            IKirajzolhato[] vissza = new IKirajzolhato[count];
            count = 0;
            foreach (var item in elemek)
                if (item is IKirajzolhato)
                    vissza[count++] = item as IKirajzolhato;
                
            return vissza;
        }
    } 
}
